const { nudgeConnectionLayer, errorLayer } = require('./layerOperations');

const createInputLayer = require('./layerOperations').createInputLayer;
const createLayer = require('./layerOperations').createLayer;
const connectLayers = require('./layerOperations').connectLayers;
const calculateLayer = require('./layerOperations').calculateLayer;
const cost = require('./nodeOperations').cost;
const error = require('./nodeOperations').error;
const nudgeValue = require('./nodeOperations').nudgeValue;
const updateConnectionLayer = require('./layerOperations').updateConnectionLayer;
const prompt = require('prompt-sync')();

function createNeuralNetwork(numInputs, numOutputs, numHiddenLayers) {
    let network = [];

    let inputLayer = createInputLayer(numInputs, 'I');

    network.push(inputLayer);

    for (let i = 0; i < numHiddenLayers; i++) {
        let hiddenLayer = [];
        let connectionLayer = [];
        let numNodes = prompt(`Enter numNodes for HiddenLayer ${i}: `);
        hiddenLayer = createLayer(numNodes, `H${i}`);

        connectionLayer = connectLayers(network[network.length - 1], hiddenLayer);

        network.push(connectionLayer);
        network.push(hiddenLayer);
    }


    let outputLayer = createLayer(numOutputs, 'O');
    let connectionLayer = connectLayers(network[network.length - 1], outputLayer);

    network.push(connectionLayer);
    network.push(outputLayer);

    return network;
}

function forwardProp(network, inputs) {

    for (let i = 0; i < network[0].length; i++) {
        network[0][i].value = inputs[i];
    }

    for (let i = 2; i < network.length; i = i + 2) {
        network[i] = calculateLayer(network[i], network[i - 1]);
        network[i - 1] = updateConnectionLayer(network[i - 1], network[i - 2], network[i]);
    }
    return network;
}

function purgeNetwork(network) {
    for (let i = 0; i < network[0].length; i++) {
        network[0][i].value = null;
    }

    for (let i = 0; i < network[network.length - 1].length; i++) {
        network[network.length - 1][i].value = null;
    }

    return network;
}

function backProp(network, expectedOutput, learningRate) {
    let costAvg = 0;
    let k = 0;

    for (let i = 0; i < network[network.length - 1].length; i++) {
        costAvg += cost(network[network.length - 1][i].value, expectedOutput[i]);
        k++;
    }

    costAvg = costAvg / k;

    for (let i = network.length - 2; i > 0; i = i - 2) {
        let errors = [];
        errors = errorLayer(network[i], costAvg, network[i + 1]);
        network[i] = nudgeConnectionLayer(network[i], errors, learningRate);
    }

    return network;
}

module.exports = { createNeuralNetwork, forwardProp, backProp, purgeNetwork };