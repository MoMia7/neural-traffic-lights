var inputNode = function (name, value) {
    var obj = {};

    obj.name = name;
    obj.value = value;

    return obj;
}

var node = function (name, bias) {
    var obj = {};

    obj.name = name;
    obj.bias = bias;
    obj.value = null;

    obj.setValue = function (value) {
        obj.value = value;
    }

    return obj;
}

var connection = function (weight, source, destination) {
    var obj = {};

    obj.weight = weight;
    obj.source = source;
    obj.destination = destination;

    return obj;
}

function calculateNode(node, connectionLayer) {
    let result = 0;
    let nodeConnections = [];

    for (let i = 0; i < connectionLayer.length; i++) {
        if (connectionLayer[i].destination.name == node.name) {
            nodeConnections.push(connectionLayer[i]);
        }
    }

    for (let i = 0; i < nodeConnections.length; i++) {
        result += nodeConnections[i].weight * nodeConnections[i].source.value;
    }

    result += node.bias;

    result = sigmoid(result);

    return result;
}

function sigmoid(x) {
    let sig = 1 / (1 + Math.pow(Math.E, -x));
    return sig;
}

function cost(predicted, expected) {
    let cost = (expected - predicted) * sigmoidDerivative(predicted);

    return cost;
}

function nudgeValue(weight, error, learningRate, sourceNodeValue) {


    let finalWeight = weight + error * sourceNodeValue * learningRate;

    return finalWeight;
}

function sigmoidDerivative(x) {
    let result = sigmoid(x) * (1 - sigmoid(x));

    return result;
}

function error(weight, cost, output) {
    let result = weight * cost * sigmoidDerivative(output);

    return result;
}

module.exports = {
    calculateNode, cost, error, nudgeValue,
    sigmoid, sigmoidDerivative, inputNode, node, connection
};