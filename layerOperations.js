const inputNode = require('./nodeOperations').inputNode;
const node = require('./nodeOperations').node;
const connection = require('./nodeOperations').connection;
const error = require('./nodeOperations').error;
const nudgeValue = require('./nodeOperations').nudgeValue;
const calculateNode = require('./nodeOperations').calculateNode;

function createInputLayer(numNodes, key) {
    let inputLayer = [];

    for (let i = 0; i < numNodes; i++) {
        let name = key + "" + i;
        inputLayer.push(inputNode(name, null));
    }

    return inputLayer;
}

function createLayer(numNodes, key) {
    let layer = [];

    for (let i = 0; i < numNodes; i++) {
        let name = key + "" + i;
        layer.push(node(name, Math.random()));
    }

    return layer;
}

function connectLayers(layer1, layer2) {
    let connections = [];

    for (let i = 0; i < layer1.length; i++) {
        for (let j = 0; j < layer2.length; j++) {
            connections.push(connection(Math.random(), layer1[i], layer2[j]));
        }
    }

    return connections;
}

function updateConnectionLayer(connectionLayer, layer1, layer2) {
    let connections = [];
    let k = 0;

    for (let i = 0; i < layer1.length; i++) {
        for (let j = 0; j < layer2.length; j++) {
            let conn = connection(connectionLayer[k].weight, layer1[i], layer2[j]);
            connections.push(conn);
            k++;
        }
    }

    return connections;
}

function calculateLayer(layer, connectionLayer) {
    for (let i = 0; i < layer.length; i++) {
        layer[i].setValue(calculateNode(layer[i], connectionLayer));
    }

    return layer;
}

function errorLayer(connectionLayer, cost) {
    let errorLayer = [];

    for (let i = 0; i < connectionLayer.length; i++) {
        errorLayer[i] = error(connectionLayer[i].weight, cost, connectionLayer[i].destination.value);
    }

    return errorLayer;
}

function nudgeConnectionLayer(connectionLayer, errors, learningRate) {
    let newConnectionLayer = connectionLayer;

    for (let i = 0; i < connectionLayer.length; i++) {
        newConnectionLayer[i].weight = nudgeValue(connectionLayer[i].weight, errors[i], learningRate, connectionLayer[i].source.value);
    }

    return newConnectionLayer;
}

module.exports = {
    createInputLayer, createLayer, connectLayers,
    updateConnectionLayer, calculateLayer, errorLayer,
    nudgeConnectionLayer
};